# @param {Integer[]} a
# @return {Integer[]}
def sorted_squares(a)
    arr = []
    a.each do |i|
        arr << i ** 2
    end
    arr.sort
end
